# README #

Here is a shell script called 'sample-tweets.sh' that accesses the in-house Twitter corpus. 
The shell script will access the twitter file from 1 March 2017 12:00 
and analyze the tweets of this specific time and answer the questions: 

- how many tweets are in the sample?

- how many unique tweets are in the sample?

- how many retweets are in the sample (out of the unique tweets)?

- in the end the script should show the first 20 unique tweets in the sample that are not retweets

There are comments in this script to document what each step is doing.